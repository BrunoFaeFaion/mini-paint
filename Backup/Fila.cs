using System;

namespace Paint
{
	/// <summary>
	/// Summary description for Fila.
	/// </summary>
	public class Fila
	{
        // Atributos da fila
        private int front, rear, max, count;
        private object[] elementos;

        // Construtor
        public Fila(int maximo)
		{
            
            this.max = maximo;
            this.front = maximo-1;
            this.rear = maximo-1;
            this.count = 0;
            this.elementos = new object[this.max];
		}

        // Se a fila est� vazia
        public bool Empty()
        {
            return (count == 0);
        }

        // Se a fila est� cheia
        public bool Full()
        {
            return (count == max);
        }

        // Insere elemento no final da fila
        public void Insert(object x)
        {
            // Checando se est� cheia
            if (Full()) throw new Exception("Fila cheia!");
            // Andando com o final da fila
            if (this.rear == this.max - 1)
                this.rear = 0;
            else
                this.rear++;
            // Inserir o elemento no final da fila
            elementos[this.rear] = x;
            this.count++;
        }

        // Remove o primeiro elemento da fila
        public object Remove()
        {
            // Checando se est� vazia
            if (Empty()) throw new Exception("Fila vazia!");
            // Andando com o come�o da fila
            if (this.front == this.max - 1)
                this.front = 0;
            else
                this.front++;
            // retornando o primeiro elemento
            this.count--;
            return this.elementos[this.front];
        }

	}
}
