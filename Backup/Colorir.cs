using System;
using System.Collections;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Paint
{
    class Pintor
    {
        #region Fun��es a implementar
        
        private Fila F;
        private Color C0;
        private Bitmap b;
        private frmPrincipal f;

        // Avalia se um ponto deve ser pintado/colocado na fila
        private void AvaliaPonto(Point PA, Color C1)
        {
            if (PA.X > 0 && PA.X < b.Width && PA.Y > 0 && PA.Y < b.Height)
            {
                Color CA = b.GetPixel(PA.X, PA.Y);
                if (CA.R == C0.R && CA.G == C0.G && CA.B == C0.B)
                {
                    b.SetPixel(PA.X, PA.Y, C1);             
                    F.Insert(PA);
                }                
            }
        }

        // Fun��o que pinta a imagem
        public void Colorir(int x, int y, Color cor, Bitmap b, frmPrincipal f)
        {
            /* Dicas: 
             *  - Uma vari�vel que armazena cor � do tipo Color. Ex.: Color cor_ponto
             *  - Pode-se colocar na fila x e y. ou pode-se colocar direto um ponto. Pode-se usar a classe Point para isso. Ex,: Point p = new Point(x,y)
             *  - Para pegar uma cor de um ponto do Bitmap use: b.GetPixel(x,y)
             *  - Para gravar uma cor em um ponto do Bitmap use: b.SetPixel(x,y,cor)
             * - Uma cor � igual a outra se seus 3 canais (R, G e B) forem iguais
             * */
            this.b = b;
            this.f = f;

            Point p = new Point(x, y);
            C0 = new Color();
            C0 = b.GetPixel(x, y);
            int area = b.Height*b.Width;

            F = new Fila(area);
            F.Insert(p);

            b.SetPixel(p.X, p.Y, cor);
            int i = 0;
            while (!F.Empty())
            {
                // Removendo ponto a ser visitado da fila
                Point P = (Point)F.Remove();

                // Coordenadas dos 4 vizinhos
                Point PL = new Point(P.X + 1, P.Y);
                Point PN = new Point(P.X, P.Y - 1);
                Point PO = new Point(P.X - 1, P.Y);
                Point PS = new Point(P.X, P.Y + 1);

                AvaliaPonto(PL, cor);    
                AvaliaPonto(PN, cor);
                AvaliaPonto(PO, cor);
                AvaliaPonto(PS, cor);
                
                // Pintar o ponto atual
                b.SetPixel(P.X, P.Y, cor);

                if (i++ % 100 == 0)
                {
                    Application.DoEvents();
                    f.pbImagem.Refresh();
                }
            }
                                            
        }

        #endregion
    }
}
