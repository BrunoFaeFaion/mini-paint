using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Paint
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmPrincipal : System.Windows.Forms.Form
	{
		#region Atributos 
        internal System.Windows.Forms.PictureBox pbImagem;
		private System.Windows.Forms.OpenFileDialog ofdImagem;
		private System.Windows.Forms.Button btnOpen;
		private System.Windows.Forms.Button btnCor;
		private System.Windows.Forms.ColorDialog cdCor;
		private System.Windows.Forms.Panel pCor;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        private Panel panel1;
        #endregion

		#region Inicializa��o
		public frmPrincipal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
            pbImagem.Image = new Bitmap(300,300);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmPrincipal());
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.pbImagem = new System.Windows.Forms.PictureBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.ofdImagem = new System.Windows.Forms.OpenFileDialog();
            this.btnCor = new System.Windows.Forms.Button();
            this.cdCor = new System.Windows.Forms.ColorDialog();
            this.pCor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagem)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbImagem
            // 
            this.pbImagem.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pbImagem.Location = new System.Drawing.Point(3, 4);
            this.pbImagem.Name = "pbImagem";
            this.pbImagem.Size = new System.Drawing.Size(472, 480);
            this.pbImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbImagem.TabIndex = 0;
            this.pbImagem.TabStop = false;
            this.pbImagem.Click += new System.EventHandler(this.pbImagem_Click);
            this.pbImagem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbImagem_MouseUp);
            // 
            // btnOpen
            // 
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Location = new System.Drawing.Point(512, 16);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(88, 32);
            this.btnOpen.TabIndex = 2;
            this.btnOpen.Text = "&Abrir...";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // ofdImagem
            // 
            this.ofdImagem.DefaultExt = "BMP";
            this.ofdImagem.Filter = "Bitmap Files (*.BMP) | *.BMP";
            // 
            // btnCor
            // 
            this.btnCor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCor.Location = new System.Drawing.Point(512, 64);
            this.btnCor.Name = "btnCor";
            this.btnCor.Size = new System.Drawing.Size(88, 32);
            this.btnCor.TabIndex = 3;
            this.btnCor.Text = "&Colorir...";
            this.btnCor.Click += new System.EventHandler(this.btnCor_Click);
            // 
            // pCor
            // 
            this.pCor.BackColor = System.Drawing.Color.Black;
            this.pCor.Location = new System.Drawing.Point(520, 112);
            this.pCor.Name = "pCor";
            this.pCor.Size = new System.Drawing.Size(72, 32);
            this.pCor.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pbImagem);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(494, 439);
            this.panel1.TabIndex = 5;
            // 
            // frmPrincipal
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(8, 19);
            this.ClientSize = new System.Drawing.Size(608, 526);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pCor);
            this.Controls.Add(this.btnCor);
            this.Controls.Add(this.btnOpen);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmPrincipal";
            this.Text = "MiniPaint";
            ((System.ComponentModel.ISupportInitialize)(this.pbImagem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		#endregion

		#region Eventos da Interface
		// Abrindo a imagem
		private void btnOpen_Click(object sender, System.EventArgs e)
		{
			if (ofdImagem.ShowDialog() == DialogResult.OK)
			{
				Bitmap b = new Bitmap(ofdImagem.FileName);
				pbImagem.Image = b;
			}
		}

		// Escolhendo a Cor
		private void btnCor_Click(object sender, System.EventArgs e)
		{
			if (cdCor.ShowDialog() == DialogResult.OK)
			{
				pCor.BackColor = cdCor.Color;
			}
            pbImagem.Cursor = Cursors.Cross;
        }

		// Chamando fun��o para colorir a partir do ponto escolhido
		private void pbImagem_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            if (pbImagem.Cursor == Cursors.Cross)
            {
                Pintor p = new Pintor();
                Bitmap b = pbImagem.Image as Bitmap;
                p.Colorir(e.X, e.Y, pCor.BackColor, b, this);
                pbImagem.Image = b;
                pbImagem.Cursor = Cursors.Arrow;
            }
		}
		#endregion

        private void pbImagem_Click(object sender, EventArgs e)
        {

        }
	}
}
