using System;
using System.Collections;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Paint
{
    class Pintor
    {            
        private Fila F;
        private Color C0;
        private Bitmap b;
        private frmPrincipal f;

                private void AvaliaPonto(Point PA, Color C1)
                {
                    if (PA.X > 0 && PA.X < b.Width && PA.Y > 0 && PA.Y < b.Height)
                    {
                        Color CA = b.GetPixel(PA.X, PA.Y);
                        if (CA.R == C0.R && CA.G == C0.G && CA.B == C0.B)
                        {
                            b.SetPixel(PA.X, PA.Y, C1);             
                            F.Insert(PA);
                        }                
                    }
                }
        private int x;
        private int y;
        private Color cor;
        public void paintPreencher() {
            Point p = new Point(x, y);
            int area = b.Height * b.Width;

            F = new Fila(area);
            F.Insert(p);

            b.SetPixel(p.X, p.Y, this.cor);
            int i = 0;
            while (!F.Empty()) {
                // Removendo ponto a ser visitado da fila
                Point P = (Point)F.Remove();

                // Coordenadas dos 4 vizinhos
                Point PL = new Point(P.X + 1, P.Y);
                Point PN = new Point(P.X, P.Y - 1);
                Point PO = new Point(P.X - 1, P.Y);
                Point PS = new Point(P.X, P.Y + 1);

                AvaliaPonto(PL, this.cor);
                AvaliaPonto(PN, this.cor);
                AvaliaPonto(PO, this.cor);
                AvaliaPonto(PS, this.cor);

                // Pintar o ponto atual
                b.SetPixel(P.X, P.Y, this.cor);

                if (i++ % 100 == 0) {
                    Application.DoEvents();
                    f.pbImagem.Refresh();
                }
            }
        }

        public void paintEspiral() {
            int area = b.Height * b.Width;

            Point p = new Point(x, y);
            Point aux = new Point();


            int planoXpositivo, planoYpositivo, k = 0, comp;
            planoXpositivo = b.Width;
            planoYpositivo = b.Height;
            Color C0 = new Color();
            C0 = b.GetPixel(x, y);

            Fila2 espiral = new Fila2(4);


            Fila2 fi = new Fila2(area);
            fi.Inserir(p);
            b.SetPixel(x, y, cor);
            k = 1;
            aux = p;
            while (!fi.FilaVazia()) {
                aux = (Point)fi.Remover();
                comp = (10 * k) + (10);

                for (int i = comp - 10 - k; i < comp; i++) {
                    if ((aux.X + 1) < planoXpositivo && (aux.X + 1) > 0 && (aux.Y) < planoYpositivo && (aux.Y) > 0) {
                        if (b.GetPixel(aux.X + 1, aux.Y).G == C0.G && b.GetPixel(aux.X + 1, aux.Y).R == C0.R && b.GetPixel(aux.X + 1, aux.Y).B == C0.B) {
                            b.SetPixel(aux.X + 1, aux.Y, cor);
                            p.X = aux.X + 1;
                            p.Y = aux.Y;
                            fi.Inserir(p);
                            aux = (Point)fi.Remover();
                        }
                    }
                }
                k++;
                comp = (10 * k) + (10);
                for (int i = comp - 10 - k; i < comp; i++) {
                    if ((aux.X) < planoXpositivo && (aux.X) > 0 && (aux.Y + 1) < planoYpositivo && (aux.Y + 1) > 0) {
                        if (b.GetPixel(aux.X, aux.Y + 1).G == C0.G && b.GetPixel(aux.X, aux.Y + 1).R == C0.R && b.GetPixel(aux.X, aux.Y + 1).B == C0.B) {

                            b.SetPixel(aux.X, aux.Y + 1, cor);
                            p.X = aux.X;
                            p.Y = aux.Y + 1;
                            fi.Inserir(p);
                            aux = (Point)fi.Remover();
                        }
                    }
                }

                k++;
                comp = (10 * k) + (10);
                for (int i = comp - 10 - k; i < comp; i++) {
                    if ((aux.X - 1) < planoXpositivo && (aux.X - 1) > 0 && (aux.Y) < planoYpositivo && (aux.Y) > 0) {
                        if (b.GetPixel(aux.X - 1, aux.Y).G == C0.G && b.GetPixel(aux.X - 1, aux.Y).R == C0.R && b.GetPixel(aux.X - 1, aux.Y).B == C0.B) {
                            b.SetPixel(aux.X - 1, aux.Y, cor);
                            p.X = aux.X - 1;
                            p.Y = aux.Y;
                            fi.Inserir(p);
                            aux = (Point)fi.Remover();
                        }
                    }
                }

                k++;
                comp = (10 * k) + (10);
                for (int i = comp - 10 - k; i < comp; i++) {
                    if ((aux.X) < planoXpositivo && (aux.X) > 0 && (aux.Y - 1) < planoYpositivo && (aux.Y - 1) > 0) {
                        if (b.GetPixel(aux.X, aux.Y - 1).G == C0.G && b.GetPixel(aux.X, aux.Y - 1).R == C0.R && b.GetPixel(aux.X, aux.Y - 1).B == C0.B) {

                            b.SetPixel(aux.X, aux.Y - 1, cor);
                            p.X = aux.X;
                            p.Y = aux.Y - 1;
                            fi.Inserir(p);
                            //aux = (Point)f.Remover();
                            if (i != comp - 1) {
                                aux = (Point)fi.Remover();
                            }
                        }
                    }
                }
                k++;
            }
        }

        public void paintTracos() {
            Point p = new Point(x, y);
            C0 = new Color();
            C0 = b.GetPixel(x, y);
            int area = b.Height * b.Width;

            F = new Fila(area);
            F.Insert(p);

            b.SetPixel(p.X, p.Y, this.cor);
            int i = 0;
            while (!F.Empty()) {
                // Removendo ponto a ser visitado da fila
                Point P = (Point)F.Remove();

                // Coordenadas dos 4 vizinhos
                Point PL = new Point(P.X + 1, P.Y + 1);
                Point PN = new Point(P.X - 3, P.Y);
                Point PO = new Point(P.X, P.Y + 3);
                Point PS = new Point(P.X, P.Y -3);

                AvaliaPonto(PL, this.cor);
                AvaliaPonto(PN, this.cor);
                AvaliaPonto(PO, this.cor);
                AvaliaPonto(PS, this.cor);

                // Pintar o ponto atual
                b.SetPixel(P.X, P.Y, this.cor);

                if (i++ % 100 == 0) {
                    Application.DoEvents();
                    f.pbImagem.Refresh();
                }
            }
        }
        public void Colorir(int x, int y, Color cor, Bitmap b, frmPrincipal f, int tipo)
        {
            this.b = b;
            this.f = f;
            this.cor = cor;
            this.x = x;
            this.y = y;
            this.C0 = new Color();
            this.C0 = b.GetPixel(x, y);
            if (C0.R == cor.R && C0.G == cor.G && C0.B == cor.B)
                return;
            if (tipo == 0) // preencher
                paintPreencher();

            else if (tipo == 1) // espiral
                paintEspiral();
            else
                paintTracos();
        }
    }
}

