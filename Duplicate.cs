using System;
using System.Collections;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Paint
{
    class Duplicate
    {
        #region Fun��es a implementar
        
        private Fila F;
        private Color C0;
        private Bitmap b;
        private frmPrincipal f;
        private static Point copyPoint = new Point(-1,-1);
        private static Point pastePoint;
        private static Bitmap bClonned;

        // Avalia se um ponto deve ser pintado/colocado na fila
        private void AvaliaPonto(Point PA, Color C1, Color fakeColor)
        {
            if (PA.X > 0 && PA.X < bClonned.Width && PA.Y > 0 && PA.Y < bClonned.Height)
            {
                Color CA = bClonned.GetPixel(PA.X, PA.Y);
                if (CA.R == C0.R && CA.G == C0.G && CA.B == C0.B)
                {
                    int pX = (int)(pastePoint.X + (PA.X - copyPoint.X));
                    int pY = (int)(pastePoint.Y + (PA.Y - copyPoint.Y));
                    if (pX >= 0 && pY >= 0 && pX < bClonned.Width && pY < bClonned.Height) {
                        
                        bClonned.SetPixel(PA.X, PA.Y, fakeColor);
                        b.SetPixel(pX, pY, C0);
                        F.Insert(PA);
                    }
                }
            }
        }
        public void callDuplicate(int x, int y, Color cor, Bitmap b, frmPrincipal f)
        {
            if (copyPoint.X==-1)
                this.Copy(x, y, cor, b, f);
            else
                this.Paste(x, y, b, f);
        }
        public void Copy(int x, int y, Color cor, Bitmap b, frmPrincipal f)
        {
            Console.WriteLine("Entrou Copy");
            this.b = b;
            this.f = f;

            copyPoint = new Point(x, y);
        }

        public void Paste(int x, int y, Bitmap b, frmPrincipal f) {
            this.b = b;
            this.f = f;
            pastePoint = new Point(x, y);
            bClonned = (Bitmap)b.Clone();
            Color fakeColor;

            Point p = new Point(copyPoint.X, copyPoint.Y);
            C0 = new Color();
            C0 = b.GetPixel(copyPoint.X, copyPoint.Y);
            int area = b.Height * b.Width;

            if (C0.R == Color.Green.R && C0.G == Color.Green.G && C0.B == Color.Green.B)
                fakeColor = Color.Blue;
            else
                fakeColor = Color.Green;

            F = new Fila(area);
            F.Insert(p);

            bClonned.SetPixel(p.X, p.Y, fakeColor);
            int i = 0;
            while (!F.Empty())
            {
                // Removendo ponto a ser visitado da fila
                Point P = (Point)F.Remove();

                // Coordenadas dos 4 vizinhos
                Point PL = new Point(P.X + 1, P.Y);
                Point PN = new Point(P.X, P.Y - 1);
                Point PO = new Point(P.X - 1, P.Y);
                Point PS = new Point(P.X, P.Y + 1);

                AvaliaPonto(PL, C0, fakeColor);
                AvaliaPonto(PN, C0, fakeColor);
                AvaliaPonto(PO, C0, fakeColor);
                AvaliaPonto(PS, C0, fakeColor);

                // Pintar o ponto atual
                bClonned.SetPixel(P.X, P.Y, fakeColor);
                b.SetPixel(pastePoint.X, pastePoint.Y, C0);

                if (i++ % 100 == 0)
                {
                    Application.DoEvents();
                    f.pbImagem.Refresh();
                }
            }
            copyPoint.X = copyPoint.Y = -1;
        }

        #endregion
    }
}
