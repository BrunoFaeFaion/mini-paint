﻿using System;

namespace Paint
{
    /// <summary>
    /// Summary description for Fila.
    /// </summary>
    public class Fila2 // função espiral
    {
        private object[] elements = new object[1000];
        private int comeco, final, tamanho, cont;
        public Fila2(int tam)
        {
            tamanho = tam;
            elements = new object[tamanho];
            comeco = 0;
            final = comeco;
            cont = 0;
        }

        public bool FilaVazia()
        {
            if (cont == 0)
            {
                return true;
            }
            return false;
        }

        public bool FilaCheia()
        {
            if (cont == tamanho)
            {
                return true;
            }
            return false;
        }
        public object Remover()
        {
            if (FilaVazia())
            {
                throw new Exception("Fila Vazia!");
            }
            if (comeco == tamanho - 1)
            {
                comeco = 0;
            }
            else
            {
                comeco++;
            }
            cont--;
            return elements[comeco];
        }

        public void Inserir(object x)
        {
            if (FilaCheia())
            {
                throw new Exception("Filha Cheia!");
            }
            if (final == tamanho - 1)
            {
                final = 0;
            }
            else
            {
                final++;
            }
            elements[final] = x;
            cont++;
        }
    }
}

