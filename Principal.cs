using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;

namespace Paint
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmPrincipal : System.Windows.Forms.Form
	{
		#region Atributos 
        internal System.Windows.Forms.PictureBox pbImagem;
		private System.Windows.Forms.OpenFileDialog ofdImagem;
        private System.Windows.Forms.SaveFileDialog svImagem;
		private System.Windows.Forms.Button btnOpen;
		private System.Windows.Forms.ColorDialog cdCor;
		private System.Windows.Forms.Panel pCor;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        private Button button1;
        private Panel panel1;
        #endregion

        private Button btnFoward;
        private ArrayList bitmaps = new ArrayList();
        private ArrayList points;
        private Button btnCopy;
        private int ImageIndex = 0;
        private int tipo = 0;
        private Button btnPre;
        private Button btnEsp;
        private Button btnTra;
        private Button btnSalvar;
        public static char option;

		#region Inicializa��o
		public frmPrincipal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
            pbImagem.Image = new Bitmap(300,300);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmPrincipal());
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.pbImagem = new System.Windows.Forms.PictureBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.ofdImagem = new System.Windows.Forms.OpenFileDialog();
            this.svImagem = new System.Windows.Forms.SaveFileDialog();
            this.cdCor = new System.Windows.Forms.ColorDialog();
            this.pCor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btnFoward = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnPre = new System.Windows.Forms.Button();
            this.btnEsp = new System.Windows.Forms.Button();
            this.btnTra = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagem)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbImagem
            // 
            this.pbImagem.BackColor = System.Drawing.Color.White;
            this.pbImagem.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pbImagem.Location = new System.Drawing.Point(0, 17);
            this.pbImagem.Name = "pbImagem";
            this.pbImagem.Size = new System.Drawing.Size(472, 480);
            this.pbImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbImagem.TabIndex = 0;
            this.pbImagem.TabStop = false;
            this.pbImagem.Click += new System.EventHandler(this.pbImagem_Click);
            this.pbImagem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbImagem_MouseUp);
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.Color.White;
            this.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Location = new System.Drawing.Point(12, 16);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(99, 32);
            this.btnOpen.TabIndex = 2;
            this.btnOpen.Text = "&Open";
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // ofdImagem
            // 
            this.ofdImagem.DefaultExt = "BMP";
            this.ofdImagem.Filter = "Bitmap Files (*.BMP) | *.BMP";
            this.svImagem.DefaultExt = "BMP";
            this.svImagem.Filter = "Bitmap Files (*.bmp) | *.bmp";
            // 
            // pCor
            // 
            this.pCor.BackColor = System.Drawing.Color.White;
            this.pCor.Location = new System.Drawing.Point(12, 92);
            this.pCor.Name = "pCor";
            this.pCor.Size = new System.Drawing.Size(99, 32);
            this.pCor.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pbImagem);
            this.panel1.Location = new System.Drawing.Point(132, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(486, 465);
            this.panel1.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(12, 188);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 32);
            this.button1.TabIndex = 6;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnFoward
            // 
            this.btnFoward.BackColor = System.Drawing.Color.White;
            this.btnFoward.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFoward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFoward.Location = new System.Drawing.Point(12, 150);
            this.btnFoward.Name = "btnFoward";
            this.btnFoward.Size = new System.Drawing.Size(99, 32);
            this.btnFoward.TabIndex = 7;
            this.btnFoward.Text = "Foward";
            this.btnFoward.UseVisualStyleBackColor = false;
            this.btnFoward.Click += new System.EventHandler(this.btnFoward_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.BackColor = System.Drawing.Color.White;
            this.btnCopy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopy.Location = new System.Drawing.Point(12, 226);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(99, 32);
            this.btnCopy.TabIndex = 8;
            this.btnCopy.Text = "Copy";
            this.btnCopy.UseVisualStyleBackColor = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnPre
            // 
            this.btnPre.BackColor = System.Drawing.Color.White;
            this.btnPre.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPre.Location = new System.Drawing.Point(12, 54);
            this.btnPre.Name = "btnPre";
            this.btnPre.Size = new System.Drawing.Size(29, 32);
            this.btnPre.TabIndex = 9;
            this.btnPre.Text = "&P";
            this.btnPre.UseVisualStyleBackColor = false;
            this.btnPre.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btnEsp
            // 
            this.btnEsp.BackColor = System.Drawing.Color.White;
            this.btnEsp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEsp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEsp.Location = new System.Drawing.Point(47, 54);
            this.btnEsp.Name = "btnEsp";
            this.btnEsp.Size = new System.Drawing.Size(29, 32);
            this.btnEsp.TabIndex = 11;
            this.btnEsp.Text = "&E";
            this.btnEsp.UseVisualStyleBackColor = false;
            this.btnEsp.Click += new System.EventHandler(this.btnEsp_Click);
            // 
            // btnTra
            // 
            this.btnTra.BackColor = System.Drawing.Color.White;
            this.btnTra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTra.Location = new System.Drawing.Point(82, 54);
            this.btnTra.Name = "btnTra";
            this.btnTra.Size = new System.Drawing.Size(29, 32);
            this.btnTra.TabIndex = 12;
            this.btnTra.Text = "&T";
            this.btnTra.UseVisualStyleBackColor = false;
            this.btnTra.Click += new System.EventHandler(this.btnTra_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.White;
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Location = new System.Drawing.Point(12, 449);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(99, 32);
            this.btnSalvar.TabIndex = 13;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(8, 19);
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(652, 519);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnTra);
            this.Controls.Add(this.btnEsp);
            this.Controls.Add(this.btnPre);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnFoward);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pCor);
            this.Controls.Add(this.btnOpen);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmPrincipal";
            this.Text = "180244 / 180607";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		#endregion

		#region Eventos da Interface
		// Abrindo a imagem
		private void btnOpen_Click(object sender, System.EventArgs e)
		{
			if (ofdImagem.ShowDialog() == DialogResult.OK)
			{
				Bitmap b = new Bitmap(ofdImagem.FileName);
                bitmaps.Clear();
                bitmaps.Add(b);
				pbImagem.Image = b;
			}
		}

		// Chamando fun��o para colorir a partir do ponto escolhido
		private void pbImagem_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            if (pbImagem.Cursor == Cursors.Cross)
            {
                Pintor p = new Pintor();
                //Bitmap b = pbImagem.Image as Bitmap;
                Bitmap b = (Bitmap) pbImagem.Image.Clone();
                pbImagem.Image = b;
                p.Colorir(e.X, e.Y, pCor.BackColor, b, this, tipo);
                if (ImageIndex != bitmaps.Count - 1)
                    for (int i = bitmaps.Count - 1; i > ImageIndex; i--)
                        bitmaps.RemoveAt(i);
                bitmaps.Add(b);
                pbImagem.Image = (Bitmap) bitmaps[bitmaps.Count-1];
                ImageIndex++;
                //pbImagem.Cursor = Cursors.Arrow;
            }
            else if (pbImagem.Cursor == Cursors.Hand)
            {
                Duplicate duplicate = new Duplicate();
                Bitmap b = (Bitmap)pbImagem.Image.Clone();
                pbImagem.Image = b;
                duplicate.callDuplicate(e.X, e.Y, pCor.BackColor, b, this);
                if (ImageIndex != bitmaps.Count - 1)
                    for (int i = bitmaps.Count - 1; i > ImageIndex; i--)
                        bitmaps.RemoveAt(i);
                bitmaps.Add(b);
                pbImagem.Image = (Bitmap)bitmaps[bitmaps.Count - 1];
                ImageIndex++;
                //pbImagem.Cursor = Cursors.Arrow;
            }
		}
		#endregion

        private void pbImagem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e) {
            if (ImageIndex > 0) {
                ImageIndex--;
                pbImagem.Image = (Bitmap)bitmaps[ImageIndex];
            }       
        }

        private void btnFoward_Click(object sender, EventArgs e) {
            if (ImageIndex < bitmaps.Count-1) {
                ImageIndex++;
                pbImagem.Image = (Bitmap)bitmaps[ImageIndex];
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            pbImagem.Cursor = Cursors.Hand;
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e) {
            if (cdCor.ShowDialog() == DialogResult.OK) {
                pCor.BackColor = cdCor.Color;
            }
            pbImagem.Cursor = Cursors.Cross;
            tipo = 0;
        }

        private void btnEsp_Click(object sender, EventArgs e) {
            if (cdCor.ShowDialog() == DialogResult.OK) {
                pCor.BackColor = cdCor.Color;
            }
            pbImagem.Cursor = Cursors.Cross;
            tipo = 1;
        }

        private void btnTra_Click(object sender, EventArgs e) {
            if (cdCor.ShowDialog() == DialogResult.OK) {
                pCor.BackColor = cdCor.Color;
            }
            pbImagem.Cursor = Cursors.Cross;
            tipo = 2;
        }

        private void btnSalvar_Click(object sender, EventArgs e) {
            String path = "";
            if (svImagem.ShowDialog() == DialogResult.OK) {
                try {
                    pbImagem.Image.Save(svImagem.FileName);
                } catch {
                    pbImagem.Image.Save("TEMP.bmp");
                    pbImagem.Load("TEMP.bmp");
                    File.Delete(svImagem.FileName);
                    pbImagem.Load(svImagem.FileName);
                    File.Delete("TEMP.bmp");
                }
            }
        }
    }
}
